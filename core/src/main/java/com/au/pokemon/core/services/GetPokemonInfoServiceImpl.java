package com.au.pokemon.core.services;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;


@Component(metatype = true,immediate = true,label = "Pokemon data")
@Service(GetPokemonInfoService.class)
public class GetPokemonInfoServiceImpl implements GetPokemonInfoService {

    @Override
    public String getPokemonData(){
        String json = "hello json";
        System.out.print("Welcome to Pokemon Service");
        try
        {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet getRequest = new HttpGet("https://pokeapi.co/api/v2/pokemon/?limit=151");
            getRequest.addHeader("accept", "application/json");
            org.apache.http.HttpResponse response = httpClient.execute(getRequest);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            String output;
            String myJSON="" ;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                myJSON = myJSON + output;
            }
            httpClient.getConnectionManager().shutdown();
            return myJSON;
        }

        catch (Exception e)
        {
            e.printStackTrace() ;
        }

        return json;
    }

}
