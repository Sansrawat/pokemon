package com.au.pokemon.core.beans;
import com.day.cq.dam.api.Asset;

public class PokemonListBean {

    private String pokemonAvatar;
    private String pokemonName;
    private int pokemonNumber=0;


    public String getPokemonAvatar() {
        return pokemonAvatar;
    }

    public void setPokemonAvatar(String pokemonAvatar) {
        this.pokemonAvatar = pokemonAvatar;
    }

    public String getPokemonName() {
        return pokemonName;
    }

    public void setPokemonName(String pokemonName) {
        this.pokemonName = pokemonName;
    }

    public int getPokemonNumber() {
        return pokemonNumber;
    }

    public void setPokemonNumber(int pokemonNumber) {
        this.pokemonNumber = pokemonNumber;
    }
}
