package com.au.pokemon.core.services;

public interface GetPokemonInfoService {
    String getPokemonData();
}
