package com.au.pokemon.core.models;

import com.adobe.cq.sightly.WCMUse;
import com.au.pokemon.core.beans.PokemonListBean;
import com.au.pokemon.core.services.GetPokemonInfoService;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class PokemonList extends WCMUse{

    private final Logger logger = LoggerFactory.getLogger(PokemonList.class);

    private List<PokemonListBean> pokemonList;

    @Inject
    GetPokemonInfoService pokemonInfoService;

    @Override
    public void activate() throws Exception {
        pokemonList = getPokemonList();
    }

    public List<PokemonListBean> getPokemonList() {
        List<PokemonListBean> pokemonListBean = new ArrayList();
        try {
            logger.info("pokemonInfoService  "+pokemonInfoService);
            if(pokemonInfoService == null) {
                pokemonInfoService = getSlingScriptHelper().getService(GetPokemonInfoService.class);
            }
            String jsonResponse = pokemonInfoService.getPokemonData();
            if(jsonResponse!=null) {
                JSONObject jsonobject = new JSONObject(jsonResponse);
                JSONArray jsonArray = (JSONArray) jsonobject.get("results");
                for(int i=1;i<=151;i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    PokemonListBean plBean = new PokemonListBean();
                    plBean.setPokemonName(jsonObject.getString("name"));
                    plBean.setPokemonAvatar("/content/dam/pokemon/images/" + i + ".png");
                    plBean.setPokemonNumber(i);
                    pokemonListBean.add(plBean);
                }
            }
        }
        catch (Exception e) {
            logger.debug("Could not get child assets using the getChildren() method. See stacktrace below\n", e);
        }
        System.out.print("pokemon data "+pokemonListBean);
        return pokemonListBean;
    }

    public List<PokemonListBean> getPokemonAvatarList() {
        return pokemonList;
    }

}
